window.Freload=function(a,a_text,b,b_text){
    if(a=='无'){a= '无'}
    else if(a=='新类别'){a= a_text}
    if(b=='无'){b= '无'}
    else if(b=='新类别'){b= b_text}
    return a+' '+b
}
setvar({

zdjlUI:object({
    name:string('').t.g,
    add:button('+')
      .H(()=>{return (typeof this.action?.type=='undefined'||this.name?.length<2)})
      .action(js(()=>{
        if($this.all .objectVars.length===1){Var.push($this.all,text('占位').desc('').size(0))}
      NB(Var.push($this.all,action(this.action).desc(Freload(this.a,this.a_text,this.b,this.b_text)).w.k.textL(self.name).T(false))).H(eval((()=>{
            let desc = $this.all .objectVars[$ID$].value.desc
            return !(($NAME$.includes(this.name)||this.name.length<2)&&
                ( ((desc.match(/\S+/)[0]==(this.a=='新类别'?this.a_text:this.a))||this.a=='无')&&
                  ((desc.match(/\S+ (\S+)/)[1]==(this.b=='新类别'?this.b_text:this.b))||this.b=='无') ))
        }).toString().replace(/\$NAME\$/g,`"${self.name}"`)
                     .replace(/\$ID\$/g,`${$this.all .objectVars.length-1}`)
        )).textR(eval((()=>{
            let desc =($this.all).objectVars[$ID$].value.desc
            let A=desc.match(/\S+/)?.[0]
            let B=desc.match(/\S+ (\S+)/)?.[1]
            let DELETE = '('+(()=>{if(confirm('确定删除?')){($this.all).objectVars[$ID$]={name:'_',value:text().size(0).out};(this.all)[$ID$]=null}}).toString()+')()'
            let RENAME = '('+(()=>{($this.all).objectVars[$ID$].value.showInputLabel=prompt('请输入',($this.all).objectVars[$ID$].value.showInputLabel)}).toString()+')()'
            let listOfTag = [['无',...new Set(Object.values($this.all .objectVars).map(i=>(i.value.desc+'').match(/\S+/)?.[0]).filter(i=>i)),'新类别'],['无',...new Set(Object.values($this.all .objectVars).map(i=>(i.value.desc+'').match(/\S+ (\S+)/)?.[1]).filter(i=>i)),'新类别']]
            window.G_UI_a_text = string(eval((()=>{return (this.a=='新类别'?(this.a_text??$TEXT$).replace(/^(新类别|无)$/,"") :this.a).replace('无',$TEXT$)}).toString().replace(/\$TEXT\$/g,JSON.stringify(A)))).t
            window.G_UI_b_text = string(eval((()=>{return (this.b=='新类别'?(this.b_text??$TEXT$).replace(/^(新类别|无)$/,"") :this.b).replace('无',$TEXT$)}).toString().replace(/\$TEXT\$/g,JSON.stringify(B)))).t
            let TAG = '('+(()=>{input=$INPUT$;setvar({G_SET:object({a:string('').t.list(input[0]),b:string('').t.list(input[1]),m:text().size(0).W(100),a_text:G_UI_a_text,b_text:G_UI_b_text})}).as('GSET').then(js(()=>{($this.all).objectVars[$ID$].value.desc=Freload(G_SET.a,G_SET.a_text,G_SET.b,G_SET.b_text)})).run}).toString().replace('$INPUT$',JSON.stringify(listOfTag))+')();'
            return `#MD [删除](javascript_then_finish:${DELETE}) [修改名称](javascript_then_finish:${RENAME}) [修改类别](javascript_then_finish:${TAG})`
                }).toString().replace(/\$NAME\$/g,`"${self.name}"`)
                             .replace(/\$ID\$/g,`${$this.all .objectVars.length-1}`)))
    })),
    action:action().W('auto'),
    a:string().t.list(()=>{return ['无',...new Set(Object.values($this.all .objectVars).map(i=>(i.value.desc+'').match(/\S+/)?.[0]).filter(i=>i)),'新类别']}),
    b:string().t.list(()=>{return ['无',...new Set(Object.values($this.all .objectVars).filter(i=>i.value.desc.includes(this.a)).map(i=>(i.value.desc+'').match(/\S+ (\S+)/)?.[1]).filter(i=>i)),'新类别']}),
    n_:text().W(100).size(1),
    save:button('保存').action(js(()=>{
        zdjl.setStorage('listSave', JSON.stringify($this.all .objectVars), 'ZDJLUI_FTHX')
        setvar({output:string(JSON.stringify($this.all .objectVars))}).run
    })),
    load:button('加载').action(js(()=>{
        let a =zdjl.getStorage('listSave', 'ZDJLUI_FTHX')
        if(a){$this.all .objectVars = JSON.parse( a)}
    })).H(()=>{return ($this.all .objectVars.length>1)}),
    loadfrom:button('导入').action(js(()=>{
        setvar({input:string('').textL('输入Url/文件路径/文本'),button_G:button('从官方服务器导入').action(js(()=>{window.input='https://gitlab.com/thx114/zdjl-UI-boot/-/raw/just-2.0/test.js'}))}).then(js(()=>{
            let a = input
            if(a.includes('http')){
                let url=a
                require('https').get(url, (res) => {let data = '';
                    res.on('data', (chunk) => {data += chunk;});
                    res.on('end', () => {zdjl.alert('导入结束');$this.all .objectVars=(JSON.parse(data+''))});
                }).on('error', (err) => {zdjl.alert('读取网络文件失败:'+err.message);});  
            }
            else if(a.includes('sdcard')||a.includes('storage')||['C','D','E','F','G','H'].includes(a[0])){
                text = require('fs').readFileSync(a)
                zdjl.alert('导入结束')
                $this.all .objectVars=(JSON.parse(text+''))
            }
            else if(a){$this.all .objectVars = JSON.parse( a)}
        })).run
    })).H(()=>{return ($this.all .objectVars.length>1)}),
    reload:button('刷新').action(js(()=>{
        $this.all .objectVars.filter(i=>i?.value?.varType==='script_action').forEach((i,index)=>{
            switch (i.value.varType){
                case 'script_action':i.value.script = Object.values(this.all).filter(i=>typeof i.type=='string')[index];break
            }
        })
    })),
    n:text('|').H(()=>{return this.a!='新类别'|this.b!='新类别'}).t.g,
    a_text:string(()=>{return this.a=='新类别'?(this.a_text??'').replace(/^(新类别|无)$/,'') :this.a}).H(()=>{return this.a!='新类别'}).t.P('right'),
    b_text:string(()=>{return this.b=='新类别'?(this.b_text??'').replace(/^(新类别|无)$/,'') :this.b}).H(()=>{return this.b!='新类别'}).t.P('right'),
    all:object({debug:text('').desc('')}).w,
})

}).runs('ZDJLUI')


